/*
 * This file is part of tupleid
 * Copyright (C) 2023  Matija Skala <mskala@gmx.com>
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef TUPLEID_HH
#define TUPLEID_HH

#include <cstdlib>
#include <string>
#include <vector>
#include <typeindex>
#ifdef __GNUC__
#include <cxxabi.h>
#endif

struct tuple_index;
template <typename... Args>
tuple_index dynamic_typeid(Args&&...);
template <typename... Args>
tuple_index static_typeid(Args&&...);

struct tuple_index {
    constexpr bool operator==(const tuple_index& other) const {
        if (std::size(m_data) != std::size(other.m_data))
            return false;
        for (std::size_t i = 0; i < std::size(m_data); i++)
            if (m_data[i] != other.m_data[i])
                return false;
        return true;
    }
    constexpr bool operator!=(const tuple_index& other) const {
        return !(*this == other);
    }
    auto hash_code() const {
        std::size_t r = 0;
        for (auto i: m_data)
            r += i.hash_code();
        return r;
    }
    std::string name() const {
        if (!std::size(m_data))
            return {};
        auto r = name(0);
        for (std::size_t i = 1; i < std::size(m_data); i++)
            r += ", " + name(i);
        return r;
    }
    std::string name(std::size_t i) const {
        return demangle(m_data.at(i).name());
    }
private:
    template <typename... Args>
    friend auto ::dynamic_typeid (Args&&...) -> tuple_index;
    template <typename... Args>
    friend auto ::static_typeid (Args&&...) -> tuple_index;
    static std::string demangle(const char* s) {
#ifdef __GNUC__
        auto tmp = abi::__cxa_demangle(s, nullptr, nullptr, nullptr);
        if (!tmp)
            return "~no type~";
        std::string r = tmp;
        std::free(tmp);
        return r;
#else
        return s;
#endif
    };
    std::vector<std::type_index> m_data;
};

template <typename... Args>
tuple_index dynamic_typeid(Args&&... args) {
    tuple_index r;
    r.m_data = {typeid(args)...};
    return r;
}

template <typename... Args>
tuple_index static_typeid(Args&&...) {
    tuple_index r;
    r.m_data = {typeid(Args)...};
    return r;
}

#endif // TUPLEID_HH
